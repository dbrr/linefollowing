# Assumptions discovered


Variables

int Mode:
	Initally set to 0
	Values: 0, 1
	SEE: MODE SELECT SWITCH CH5
	Uses 2 if statements, can be optimised to if else

	When input 	E is value 	100, 	set to 	mode 0
	When input 	E is value 	-100, 	set to 	mode 1
	This will print a line in console of value E and current mode in use when used 

int start:
int lineF:
	Initally set to 0
	Values: 0, 1
	
	When set to mode 0 & value then sets to 1 will run:
		lfsetup()
		lfloop()
		and set lineF to 1 again

		int count will never reach 30 due to _delay function run in move() function
		when _delay() is run, will run loop() function again
		this then causes lfsetup() to be run which resets count


# Mode select switch (Channel 5)

## Mode 0
This looks like where the line follower program is run

### functions:

lfsetup():	line follower setup
lfloop():	line follower loop


## Mode 1
This looks like it will be manually operated
