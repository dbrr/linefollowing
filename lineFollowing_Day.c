
/* 
	RAN ROVER REMOTE CONTROL
	AUTHOR: Jordan Day
			Matthew Teis
*/

#include "MeMegaPi.h"
#include "arduino.h"				//li
#include "Wire.h"					//li
#include "SoftwareSerial.h"			//li
#include "lineFollwerFunctions.c"


MeLineFollower linefollower_8(8);	//li
double angle_rad = PI/180.0;		//li
double angle_deg = 180.0/PI;		//li
MeMegaPiDCMotor motor1(PORT4A);
MeMegaPiDCMotor motor2(PORT4B);
MeMegaPiDCMotor motor3(PORT3A);
MeMegaPiDCMotor motor4(PORT3B);
MeMegaPiDCMotor motor5(PORT2A);
MeMegaPiDCMotor motor6(PORT2B);
MeMegaPiDCMotor motor7(PORT1A);
MeMegaPiDCMotor motor8(PORT1B);
MeEncoderOnBoard Encoder_1(SLOT1);	//li
MeEncoderOnBoard Encoder_2(SLOT2);	//li



uint8_t motorSpeed = 200;


int Mode = 0;
int start = 0;
int lineF = 0;

float count = 0;					//li
float Speed = 0;					//li

int ch1;	// Here's where we'll keep our channel values
int ch2;
int ch3;
int ch4;
int ch5;



void loop() {

  ch1 = pulseIn(30, HIGH, 100000); // Read the pulse width of
  ch2 = pulseIn(28, HIGH, 100000); // each channel
  ch3 = pulseIn(26, HIGH, 100000);
  ch4 = pulseIn(24, HIGH, 100000);
  ch5 = pulseIn(22, HIGH, 100000);






  /////////////////////////////MODE SELECT SWITCH CH5////
  Serial.print("  Channel 5 Switch:  "); // Ch5 Switch
  int E = map(ch5, 1000, 2000, -255, 255); // center at 0
  Serial.println(E); // center at 0
  if (E > 100) {
    Mode = 0;
    Serial.println("Mode 0");
  }
  if (E < -100) {
    Mode = 1;
    Serial.println("Mode 1");
  }
  //////////////////////////////////////////

    
  if (Mode == 0) {
    ///////////////////////left stick x//////////////////////
    Serial.print("LEFT STICK up down C:"); // Ch3 was x-axis
     start = 2;
    int C = map(ch3, 1000, 2000, -255, 255); // center at 0
    // C deadzone
    if ((C < 50) && (C > -50)) {
      C = 0;
    }
    Serial.print(C); // center at 0
    if (C > 50) { // move forward
      forward(C);
    }
    if (C < -50) { // move forward
      reverse(C);
    }
    ////////////////////////



    ///////////////////////left stick y/////////////////////////
    Serial.print("  LEFT STICK  LEFT/RIGHT D:"); // Ch3 was x-axis
    int D = map(ch4, 1000, 2000, -255, 255); // center at 0
    // D deadzone
    if ((D < 50) && (D > -50)) {
      D = 0;
    }
    Serial.println(D); // center at 0
    if (D > 50) { //move left
      LEFT(D);
    }
    if (D < -50) { //move left
      RIGHT(D);
    }
    ////////////////////////

    /////////////////STOP///////////////////
    if ((C == 0) && (D == 0)) {
      stop();
    }
    ///////////////////////
 

   Serial.print("  RIGHT Stick  UP/DOWN A:"); // Ch3 was x-axis
   ////  int A = map(ch2, 1000, 2000, -255, 255); // center at 0
 ////   // A deadzone
 ////   if ((A < 50) && (A > -50)) {
 ////     A = 0;
 ////   }
////    Serial.println(A); // center at 0
 ////   motor1.run(A);
    //////////////////////////////////////////

    /////////////////////////////LEFT STICK UP/DOWN////
    ////Serial.print("  RIGHT Stick  LEFT/ RIGHT B:"); // Ch3 was x-axis
////    int B = map(ch1, 1000, 2000, -255, 255); // center at 0
////    // B deadzone
////    if ((B < 50) && (B > -50)) {
////      B = 0;
////    }
  ////  Serial.println(B); // center at 0
////    motor3.run(B);
    //////////////////////////////////////////
    int A = map(ch2, 1000, 2000, -255, 255);
    if ((A < 50) && (A > -50)) {
      A = 0;
    } 
    Serial.println(A); // center at 0
    int B = map(ch1, 1000, 2000, -255, 255);
    if ((B < 50) && (B > -50)) {
      B = 0;
    }
    Serial.print("  RIGHT Stick  LEFT/ RIGHT B:"); // Ch3 was x-axis
    Serial.println(B); // center at 0
        if ((A > 50) && (B > 50)) {
       lineF = 1;
      }
    if (lineF == 1) {
      lfsetup();
      lfloop();
      lineF = 1;
    }
} //end of mode 0   
              




  if ((Mode == 1) && (start == 2)) {
    ///////////////////////LEFT stick x//////////////////////
    Serial.print("LEFT Stick up down C:"); // Ch3 was x-axis
    int C = map(ch3, 1000, 2000, -255, 255); // center at 0
    // C deadzone
    if ((C < 50) && (C > -50)) {
      C = 0;
    }
    Serial.print(C); // center at 0
    motor2.run(C);

    ////////////////////////



    ///////////////////////LEFT stick y/////////////////////////
    Serial.print("  LEFT Stick  LEFT/RIGHT D:"); // Ch3 was x-axis
    int D = map(ch4, 1000, 2000, -255, 255); // center at 0
    // D deadzone
    if ((D < 50) && (D > -50)) {
      D = 0;
    }
    Serial.println(D); // center at 0
    motor4.run(D);

    ////////////////////////


    /////////////////////////////RIGHT STICK UP/DOWN////
    Serial.print("  RIGHT Stick  UP/DOWN A:"); // Ch3 was x-axis
    int A = map(ch2, 1000, 2000, -255, 255); // center at 0
    // M deadzone
    if ((A < 50) && (A > -50)) {
      A = 0;
    }
    Serial.println(A); // center at 0
    motor1.run(-A);
    //////////////////////////////////////////

    /////////////////////////////RIGHT STICK LEFT/RIGHT////
    Serial.print("  RIGHT Stick  LEFT/RIGHT B:"); // Ch3 was x-axis
    int B = map(ch1, 1000, 2000, -255, 255); // center at 0
    // B deadzone
    if ((B < 50)
        && (B > -50)) {
      B = 0;
    }
    Serial.println(B); // center at 0
    motor3.run(B);
    //////////////////////////////////////////

  } //end of mode 1











  Serial.println(); //make some room

  delay(100);// I put this here just to make the terminal
  // window happier
}
