# Engineering Challenge - Line Follower Program

Line Follower For Arduino Robot - Engineering Challenge



# Pseudo Code


## Case 1 - Sensor does not detect black line

This case is where both sensors are detecting black

``` C
	function(drive forward) {
		motorLeft(forward);
		motorRight(forward);
	}
```

## Case 2 - Left/ Right sensor detects black line

This case, the robot must turn left/right when white is detected only on one sensor

``` C
	if(left motor detects black line) {
		function(turn right) {
			when white detected;
			return(to case 1);
		}
	}
	if(right motor detects black line) {
		function(turn right) {
			when no black line detected;
			return(to case 1)
		}
	}
```

## Case 3 - Both sensors detect black line

NOTE: THIS CASE SHOULD NOT BE NEEDED

``` C
	// Check if finish line or line continues through course
	If (both sensors detect line) {
		// Roll past black line
		Drive Forward 1/4 rotation;
		
		// Turn to check for black line
		function(Rotate left/right 45 - 90 degrees) {
			if (black line is detected) {
				Rotate 45 - 90 degrees back to origional orientation;
				return (to line following program); // Go to case 1
			}
			// No black line was found, Return back to finish line
			else {
				Rotate 45 - 90 degrees back to origional orientation;
				Reverse 1/4 rotation;
			}
		}
	}
