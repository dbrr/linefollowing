
/* RAN ROVER REMOTE CONTROL for Makeblock MegaPi
  by www.nuvotion.com.au
  littlebird.com.au
  modified 22/10/2018
  by NICK OWEN
  Version 1.0 Block A
*/
#include "MeMegaPi.h"
#include "arduino.h"//li
#include "Wire.h"//li
#include "SoftwareSerial.h"//li

MeLineFollower linefollower_8(8);//li
double angle_rad = PI/180.0;//li
double angle_deg = 180.0/PI;//li
MeMegaPiDCMotor motor1(PORT4A);
MeMegaPiDCMotor motor2(PORT4B);
MeMegaPiDCMotor motor3(PORT3A);
MeMegaPiDCMotor motor4(PORT3B);
MeMegaPiDCMotor motor5(PORT2A);
MeMegaPiDCMotor motor6(PORT2B);
MeMegaPiDCMotor motor7(PORT1A);
MeMegaPiDCMotor motor8(PORT1B);
MeEncoderOnBoard Encoder_1(SLOT1);//li
MeEncoderOnBoard Encoder_2(SLOT2);//li


uint8_t motorSpeed = 200;

int Mode = 0;
int start = 0;
int lineF = 0;

float count = 0;//li
float Speed = 0;//li

int ch1; // Here's where we'll keep our channel values
int ch2;
int ch3;
int ch4;
int ch5;


void setup() {
  pinMode(24, INPUT); // Set our input pins as such
  pinMode(26, INPUT); // Set our input pins as such
  pinMode(28, INPUT);
  pinMode(30, INPUT);

  Serial.begin(9600); // Pour a bowl of Serial

}

void loop() {

  ch1 = pulseIn(30, HIGH, 100000); // Read the pulse width of
  ch2 = pulseIn(28, HIGH, 100000); // each channel
  ch3 = pulseIn(26, HIGH, 100000);
  ch4 = pulseIn(24, HIGH, 100000);
  ch5 = pulseIn(22, HIGH, 100000);






  /////////////////////////////MODE SELECT SWITCH CH5////
  Serial.print("  Channel 5 Switch:  "); // Ch5 Switch
  int E = map(ch5, 1000, 2000, -255, 255); // center at 0
  Serial.println(E); // center at 0
  if (E > 100) {
    Mode = 0;
    Serial.println("Mode 0");
  }
  if (E < -100) {
    Mode = 1;
    Serial.println("Mode 1");
  }
  //////////////////////////////////////////

    
  if (Mode == 0) {
    ///////////////////////left stick x//////////////////////
    Serial.print("LEFT STICK up down C:"); // Ch3 was x-axis
     start = 2;
    int C = map(ch3, 1000, 2000, -255, 255); // center at 0
    // C deadzone
    if ((C < 50) && (C > -50)) {
      C = 0;
    }
    Serial.print(C); // center at 0
    if (C > 50) { // move forward
      forward(C);
    }
    if (C < -50) { // move forward
      reverse(C);
    }
    ////////////////////////



    ///////////////////////left stick y/////////////////////////
    Serial.print("  LEFT STICK  LEFT/RIGHT D:"); // Ch3 was x-axis
    int D = map(ch4, 1000, 2000, -255, 255); // center at 0
    // D deadzone
    if ((D < 50) && (D > -50)) {
      D = 0;
    }
    Serial.println(D); // center at 0
    if (D > 50) { //move left
      LEFT(D);
    }
    if (D < -50) { //move left
      RIGHT(D);
    }
    ////////////////////////
    

    ///////////////////////STOP/////////////////////////
    if ((C == 0) && (D == 0)) {
      stop();
    }
    
 

   Serial.print("  RIGHT Stick  UP/DOWN A:"); // Ch3 was x-axis
/*	Found this code not in use - JD
     int A = map(ch2, 1000, 2000, -255, 255); // center at 0
    // A deadzone
    if ((A < 50) && (A > -50)) {
      A = 0;
    }
    Serial.println(A); // center at 0
    motor1.run(A);

	//LEFT STICK UP/DOWN////
    Serial.print("  RIGHT Stick  LEFT/ RIGHT B:"); // Ch3 was x-axis
    int B = map(ch1, 1000, 2000, -255, 255); // center at 0
    // B deadzone
    if ((B < 50) && (B > -50)) {
      B = 0;
    }
    Serial.println(B); // center at 0
    motor3.run(B);
*/
    int A = map(ch2, 1000, 2000, -255, 255);
    if ((A < 50) && (A > -50)) {
      A = 0;
    } 
    Serial.println(A); // center at 0
    int B = map(ch1, 1000, 2000, -255, 255);
    if ((B < 50) && (B > -50)) {
      B = 0;
    }
    Serial.print("  RIGHT Stick  LEFT/ RIGHT B:"); // Ch3 was x-axis
    Serial.println(B); // center at 0
        if ((A > 50) && (B > 50)) {
       lineF = 1;
      }
    if (lineF == 1) {
      lfsetup();
      lfloop();
      lineF = 1;
    }
}
//end of mode 0   
              




  if ((Mode == 1) && (start == 2)) {
    ///////////////////////LEFT stick x//////////////////////
    Serial.print("LEFT Stick up down C:"); // Ch3 was x-axis
    int C = map(ch3, 1000, 2000, -255, 255); // center at 0
    // C deadzone
    if ((C < 50) && (C > -50)) {
      C = 0;
    }
    Serial.print(C); // center at 0
    motor2.run(C);
    ////////////////////////


    ///////////////////////LEFT stick y/////////////////////////
    Serial.print("  LEFT Stick  LEFT/RIGHT D:"); // Ch3 was x-axis
    int D = map(ch4, 1000, 2000, -255, 255); // center at 0
    // D deadzone
    if ((D < 50) && (D > -50)) {
      D = 0;
    }
    Serial.println(D); // center at 0
    motor4.run(D);
    ////////////////////////


    /////////////////////////////RIGHT STICK UP/DOWN////
    Serial.print("  RIGHT Stick  UP/DOWN A:"); // Ch3 was x-axis
    int A = map(ch2, 1000, 2000, -255, 255); // center at 0
    // M deadzone
    if ((A < 50) && (A > -50)) {
      A = 0;
    }
    Serial.println(A); // center at 0
    motor1.run(-A);
    //////////////////////////////////////////

    /////////////////////////////RIGHT STICK LEFT/RIGHT////
    Serial.print("  RIGHT Stick  LEFT/RIGHT B:"); // Ch3 was x-axis
    int B = map(ch1, 1000, 2000, -255, 255); // center at 0
    // B deadzone
    if ((B < 50)
        && (B > -50)) {
      B = 0;
    }
    Serial.println(B); // center at 0
    motor3.run(B);
    //////////////////////////////////////////

  } //end of mode 1











  Serial.println(); //make some room

  delay(100);// I put this here just to make the terminal
  // window happier
}

void forward(int speed) {

  motor6.run(-speed); /* value: between -255 and 255. */

  motor8.run(speed); /* value: between -255 and 255. */
  Serial.println("FWD"); //
}

void reverse(int speed) {

  motor6.run(-speed); /* value: between -255 and 255. */

  motor8.run(speed); /* value: between -255 and 255. */
  Serial.println("REV"); //

}
void stop(void) {

  motor6.run(0); /* value: between -255 and 255. */

  motor8.run(0); /* value: between -255 and 255. */
}


void LEFT(int speed) {
  motor6.run(-speed); /* value: between -255 and 255. */
  motor8.run(-speed); /* value: between -255 and 255. */
}

void RIGHT(int speed) {
  motor6.run(-speed); /* value: between -255 and 255. */
  motor8.run(-speed); /* value: between -255 and 255. */
}

void move(int direction, int speedlf) {
        int leftSpeed = 0;
        int rightSpeed = 0;
        if(direction == 1){			
          leftSpeed = -1 * speedlf;
          rightSpeed = speedlf;
        }else if(direction == 2){
          leftSpeed = speedlf;
          rightSpeed = -1 * speedlf;
        }else if(direction == 3){
          leftSpeed = speedlf;
          rightSpeed = speedlf;
        }else if(direction == 4){
          leftSpeed = -1 * speedlf;
          rightSpeed = -1 * speedlf;
        }
        Encoder_1.setTarPWM(rightSpeed);
        Encoder_2.setTarPWM(leftSpeed);
      }
      
void isr_process_encoder1(void){
        if(digitalRead(Encoder_1.getPortB()) == 0){
          Encoder_1.pulsePosMinus();
        }else{
          Encoder_1.pulsePosPlus();
        }
      }
             
void isr_process_encoder2(void){
        if(digitalRead(Encoder_2.getPortB()) == 0){
          Encoder_2.pulsePosMinus();
        }else{
          Encoder_2.pulsePosPlus();
        }
      }       
      
void _delay(float seconds) {
        long endtime = millis() +seconds * 1000;
        while(millis() < endtime) _loop();
      }  

void lfsetup() {
        attachInterrupt(Encoder_1.getIntNum(), isr_process_encoder1,RISING);
        attachInterrupt(Encoder_2.getIntNum(), isr_process_encoder2,RISING);
        TCCR1A = _BV(WGM10);
        TCCR1B = _BV(CS11) | _BV(WGM12);
        TCCR2A = _BV(WGM21) | _BV(WGM20);
        TCCR2B = _BV(CS21);
        count = 0;
        Speed = 25;
      }

void _loop() {
        Encoder_1.loop();
        Encoder_2.loop();
      }

void lfloop() {

		/* Found from online:
			Sensor:
				0x00 sensor1 & sensor 2 inside black line
				0x01 sesor1		inside		sensor2		outside 
				0x02 sesor1		outside		sensor2		inside 
				0x03 sensor1 & sensor 2 ouside black line
		*/

		/*
		 function: move(int direction, int speedlf) 
			direction 1:
				L	speed - 1
				R	no change
				moves robot left

			direction 2:
				L	no change
				R	speed - 1
				moves robot right

			direction 3:
				L	no change
				R	no change
				both motor speeds are same

			direction 4:
				L	speed - 1
				R	speed - 1
				Slows robot
		*/

		// TODO: can this robot move left? no move(1,speed) found

        Serial.print("  count ");
        Serial.print(count);
        if(linefollower_8.readSensors() == 0.0){
			// TODO: find why this if statement has no operations
			// TODO: What is sensor value 0.0
			// TODO: Find sensor value meanings

        }else{
          if(linefollower_8.readSensors() == 1){
            move(3,Speed / 100 *255); 		// move fwd, no speed change
            
          }else{
          if(linefollower_8.readSensors() == 2){
            move(4,Speed / 100 *255); 		// decrease speed

            }else{
              if ((linefollower_8.readSensors() == 3) && (count < 30)){
                move(2,Speed / 100 * 255); 	// TODO:should this be moving right?
                _delay(0.4);
                move(2,0);
                count = count + 1;

              }else{
                  if((linefollower_8.readSensors() == 3) && (count == 30)){
                    Encoder_1.setTarPWM(0);
                    Encoder_2.setTarPWM(0);
                     
                     }

               }

             }

           }

         }

    _loop();
}
